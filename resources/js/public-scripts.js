function x_dokan_bling_atualiza_config_token(token_bling){

    var data = {
        'action': 'dokan_bling_config_token',
        'token_bling': token_bling,
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    jQuery.post(bling_ajax_object.ajax_url, data, function(response) {
        if(response){
            // console.log(response);
            alert(response);
        }

    });    
}
function x_dokan_bling_importa_produtos(token_bling){

    var data = {
        'action': 'dokan_bling_importa_produtos',
        'token_bling': token_bling,
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    jQuery.post(bling_ajax_object.ajax_url, data, function(response) {
        if(response){
            // console.log(response);
            alert(response);
        }

    });    
}

jQuery(function() {
    jQuery('body').on("click", "#importar_bling", function(){
            var token_bling = jQuery( "#token_bling" ).val();
            x_dokan_bling_importa_produtos(token_bling);      
    });
});