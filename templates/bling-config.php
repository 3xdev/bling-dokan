<?php
/**
 *  Dokan Dashboard Template
 *
 *  Dokan Main Dahsboard template for Fron-end
 *
 *  @since 2.4
 *
 *  @package dokan
 */
?>
<div class="dokan-dashboard-wrap">
    <?php
        /**
         *  dokan_dashboard_content_before hook
         *
         *  @hooked get_dashboard_side_navigation
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_before' );
    ?>

    <div class="dokan-dashboard-content">

        <?php
            /**
             *  dokan_dashboard_content_before hook
             *
             *  @hooked show_seller_dashboard_notice
             *
             *  @since 2.4
             */
            do_action( 'dokan_help_content_inside_before' );
        ?>
         <?php 
            $currentUser = wp_get_current_user();
            
            // print_r($current_zip['address']['zip']);
        ?>
        <article class="help-content-area">
        	<h1>Configure seu integração com a Bling</h1>
          	<p>
                Para realizar a integração da sua conta bling informe a sua chave de integração, você pode gerar uma chave de API
                acessando a página de usuários na sua conta bling e adicionar um usuário API.

                A integração Bling permite:
                <ol>
                    <li>Exportação de pedidos automática</li>
                    <li>Importação de produtos da sua loja Bling na tela de produtos</li>
                </ol>

                Cole aqui sua chave de api para habilitar estes recursos.
            </p>
            <form role="form" method="post">
            <div class="dokan-form-group">
                <label for="token-bling">Sua chave de api Bling</label>
                <input class="dokan-form-control" id="token-bling" type="text" name="token-bling" value="<?php echo get_user_meta($currentUser->ID, '_3x_dokan_token_bling', true); ?>">
            </div>
            <div class="dokan-form-group">
                <button type="submit" id="bling_action" class="button button-small">Salvar</button>
            </div>
            </form>
            

        </article><!-- .dashboard-content-area -->
        <script type="text/javascript">
            jQuery(function() {
            // console.log( "pronto para executar o js!" );
            jQuery('body').on("click", "#bling_action", function(e){
                e.preventDefault();
                    
                    var token_bling = jQuery( "#token-bling" ).val();

                 x_dokan_bling_atualiza_config_token(token_bling);      
            });

        });
        </script>
         <?php
            /**
             *  dokan_dashboard_content_inside_after hook
             *
             *  @since 2.4
             */
            do_action( 'dokan_dashboard_content_inside_after' );
        ?>


    </div><!-- .dokan-dashboard-content -->

    <?php
        /**
         *  dokan_dashboard_content_after hook
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_after' );
    ?>

</div><!-- .dokan-dashboard-wrap -->