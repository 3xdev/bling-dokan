<?php
// insere o botao de importar na tela de pedidos
add_action( 'dokan_after_add_product_btn', 'importar_produtos_bling', 10 );

function importar_produtos_bling(){
    $currentUser = wp_get_current_user();
	?>
	<script type="text/javascript">
	</script>
	<a  id="importar_bling" class="dokan-btn dokan-btn-theme"><i class="fa fa-link">&nbsp;</i>Importar do bling</a>
    <input type="hidden" value="<?php echo get_user_meta($currentUser->ID, '_3x_dokan_token_bling', true); ?>" id="token_bling">
	<?php
} 