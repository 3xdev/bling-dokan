<?php
defined( 'ABSPATH' ) || exit;

// Primeiro definimos os status customizados, isso vai nos ajudar a manter os pedidos dos lojista
// separados da conta principal, que vai usar a api do bling tradicional.

function _3x_register_bling_processing_seller() {
    register_post_status( 'wc-fornecedor-processando', array(
        'label'                     => 'Fornecedor Processando',
        'public'                    => true,
        'show_in_admin_status_list' => true,
        'show_in_admin_all_list'    => true,
        'exclude_from_search'       => false,
        'label_count'               => _n_noop( 'Fornecedor Processando <span class="count">(%s)</span>', 'Fornecedor Processando <span class="count">(%s)</span>' )
    ) );

    register_post_status( 'wc-fornecedor-concluido', array(
        'label'                     => 'Fornecedor Concluído',
        'public'                    => true,
        'show_in_admin_status_list' => true,
        'show_in_admin_all_list'    => true,
        'exclude_from_search'       => false,
        'label_count'               => _n_noop( 'Fornecedor Concluído <span class="count">(%s)</span>', 'Fornecedor Concluído <span class="count">(%s)</span>' )
    ) );
}
add_action( 'init', '_3x_register_bling_processing_seller' );

function _3x_adiciona_status_processando_bling( $order_statuses ) {
    $new_order_statuses = array();
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-fornecedor-processando'] = 'Fornecedor Processando';
        }
        if ( 'wc-completed' === $key ) {
            $new_order_statuses['wc-fornecedor-concluido'] = 'Fornecedor Concluído';
        }
    }
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', '_3x_adiciona_status_processando_bling' );


// executa a funcao sempre que o status de um pedido for alterado 
add_action('woocommerce_order_status_changed','dokan_bling_status_change_custom', 10 , 3);

// Esta função é disparada quando o status do pedido é alterado para "processando" ou "concluído"
// Sempre que um pedido for receber o status e tiver um vendedor vinculado, ele então coloca o status equivalente
// Isso deve impedir que os pedidos sejam enviados para o erp da loja principal por acidente
function dokan_bling_status_change_custom($order_id, $old_status, $new_status) {
    
    if ( ! $order_id ) {return;}  

    if($new_status === "processing" || $new_status === "completed"){

        $order = new WC_Order( $order_id );
        $sellers = dokan_get_seller_id_by_order( $order );

        // verifica se o pedido tem sub-pedidos, se não tiver subpedidos pula
        if ( $order->get_meta('has_sub_order') ) {
                return;

        } else {
            // pega os dados do único vendedor
            $seller_info = get_userdata( $sellers );
            $store_info  = dokan_get_store_info( $seller_info->ID );
            $seller_token = get_user_meta( $seller_info->ID, '_3x_dokan_token_bling', true );
            
            if( 'processing' == $order->get_status() ) {
                $order->update_status( 'wc-fornecedor-processando' );
            }
            if ( 'completed' == $order->get_status() ) {
                $order->update_status( 'wc-fornecedor-concluido' );
            }
            // Aqui faz a chamada para a API externa para enviar o pedido ao lojista
            $order = (new _3X_bling_process)->enviar_pedido($seller_token, $order);

            if( is_wp_error( $order ) ) {
                error_log( json_encode($order) );
            }else{
                error_log( json_encode($order) );
            }
        }
    }
}