<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function dokan_bling_atualiza_config_token(){

	if(!isset($_POST)){
		wp_die();
	}
    
    if(!isset($_POST['token_bling']) || $_POST['token_bling'] == ''){
    	echo 'Dados imcompletos, por favor preencha todos os campos corretamente.';
    	wp_die();
    }

	$token_bling =  sanitize_text_field( $_POST['token_bling'] );

	// $current_zip = get_user_meta($currentUser->ID, 'dokan_profile_settings', true);
	$currentUser = wp_get_current_user();

 	$updateToken = update_user_meta($currentUser->ID, '_3x_dokan_token_bling', $token_bling);

	echo 'Dados atualizados'; 		
 	
	// echo $token_bling;
	

	wp_die(); // this is required to terminate immediately and return a proper response
        
}

add_action('wp_ajax_dokan_bling_config_token', 'dokan_bling_atualiza_config_token');
add_action('wp_ajax_nopriv_dokan_bling_config_token', 'dokan_bling_atualiza_config_token');

function dokan_bling_importa_produtos(){
	if(!isset($_POST)){
		wp_die();
	}
    if(empty($_POST['token_bling']) || !isset($_POST['token_bling'])){
        echo 'Você precisa informar o token API na tela de configuração';
        wp_die();
    }
	$token_bling = sanitize_text_field($_POST['token_bling']);
    
	$json_produtos = (new _3X_bling_process)->processa_produtos($token_bling);
    
    if( $json_produtos ){
        echo 'sucesso na importação!';
        wp_die();
    }else{
        echo 'falha na importação!';
        wp_die();
    }
	

	
}
add_action('wp_ajax_dokan_bling_importa_produtos', 'dokan_bling_importa_produtos');
add_action('wp_ajax_nopriv_dokan_bling_importa_produtos', 'dokan_bling_importa_produtos');