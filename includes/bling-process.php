<?php
/**
 * summary
 */
class _3X_bling_process {
    /**
     * summary
     */
    public function __construct(){
        
    }
    
    public function get_remote_images($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $picture = curl_exec($ch);
        curl_close($ch);
        
        header('Content-type: image/jpeg');

        return $picture;
    }
    
    public function processa_produtos($apikey){

        $url = 'https://bling.com.br/Api/v2/produtos/json';

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url . '&apikey=' . $apikey. '&imagem=S');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curl_handle);
        curl_close($curl_handle);
        
        $arr = json_decode($response, true);

        foreach($arr['retorno']['produtos'] as $produto) {

            // $imagens = $produto['produto']['imagem'];
            
            // foreach($imagens as $imagem){
            //     $product_photos[] = [ 'src' => self::get_remote_images($imagem['link']) ];
            // }
            // error_log(json_encode($product_photos));

            $data = [
                'type' => 'simple',
                'visible' => true,
                'name' => $produto['produto']['descricao'],
                'description' => $produto['produto']['descricaoCurta'],
                'short_description' => $produto['produto']['descricaoComplementar'],
                'regular_price' => $produto['produto']['preco'],
                'sku' => $produto['produto']['codigo'],
                'weight' => $produto['produto']['pesoLiq'],
                'dimensions' => [
                    'length' => $produto['produto']['profundidadeProduto'],
                    'width' => $produto['produto']['larguraProduto'],
                    'height' => $produto['produto']['alturaProduto'],
                ],
                // 'images' => 
                //     $product_photos
                

            ];
            error_log(json_encode($data));
            
            $request = new WP_REST_Request( 'POST' );
            $request->set_body_params( $data );
            $products_controller = new WC_REST_Products_Controller;
            
            // se nao existir cria o item
            $response = $products_controller->create_item( $request );
            // se der um erro no cadastro
            if( is_wp_error( $response ) ) {
                // se ja existir o produto vamos atualizar
                if($response->get_error_message() === 'Invalid or duplicated SKU.'){
                    // pega o ID do produto pra atualizar
                    
                    $data_to_update = [
                        'id' => wc_get_product_id_by_sku( $produto['produto']['codigo'] ),
                        'type' => 'simple',
                        'visible' => true,
                        'name' => $produto['produto']['descricao'],
                        'description' => $produto['produto']['descricaoCurta'],
                        'short_description' => $produto['produto']['descricaoComplementar'],
                        'regular_price' => $produto['produto']['preco'],
                        'sku' => $produto['produto']['codigo'],
                        'weight' => $produto['produto']['pesoLiq'],
                        'dimensions' => [
                            'length' => $produto['produto']['profundidadeProduto'],
                            'width' => $produto['produto']['larguraProduto'],
                            'height' => $produto['produto']['alturaProduto'],
                        ]
                    ];
                    error_log(json_encode($data_to_update));

                    $request = new WP_REST_Request( 'PUT' );
                    $request->set_body_params( $data_to_update );
                    
                    $response = $products_controller->update_item( $data_to_update );
                    if( is_wp_error( $response ) ) {
                        return json_encode($response->get_error_message());
                    }

                }else{
                    return  error_log(json_encode($response->get_error_message()));
                }
            }

        }
        
        return $response;
    }

    public function enviar_pedido($apikey, $order){
        // Get and Loop Over Order Items
        $produtos = '';
        foreach ( $order->get_items() as $item_id => $item ) {
            $product = wc_get_product($item->get_product_id());
            $produtos .= '<item>
            <codigo>'.$product->get_sku().'</codigo>
            <descricao>'.$item->get_name().'</descricao>
            <qtde>'.$item->get_quantity().'</qtde>
            <vlr_unit>'.$product->get_price().'</vlr_unit>
            </item>';
        }
        error_log('apikey: '.$apikey);
        //preciso receber os dados do pedido também 
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
        <pedido>
         <cliente>
            <nome>'.$order->get_formatted_billing_full_name().'</nome>
            <tipoPessoa>J</tipoPessoa>
            <endereco>'.$order->get_billing_address_1().'</endereco>
            <cpf_cnpj>00000000000000</cpf_cnpj>
             <ie>3067663000</ie>
            <numero>392</numero>
            <complemento>'.$order->get_billing_address_2().'</complemento>
            <bairro>Cidade Alta</bairro>
            <cep>'.$order->get_billing_postcode().'</cep>
            <cidade>Bento Gonçalves</cidade>
            <uf>'.$order->get_billing_state().'</uf>
            <fone>'.$order->get_billing_phone().'</fone>
            <email>'.$order->get_billing_email().'</email>
         </cliente>
         <itens>
            '.$produtos.'
         </itens>
         <parcelas>
            <parcela>
                <data>'.$order->get_date_paid().'</data>
                <vlr>'.$order->get_total().'</vlr>
            </parcela>
         </parcelas>
         <vlr_frete>'.$order->get_shipping_total().'</vlr_frete>
         <vlr_desconto>'.$order->get_total_discount().'</vlr_desconto>
         <obs>Testando o campo observações do pedido</obs>
         <obs_internas>Testando o campo observações internas do pedido</obs_internas>
        </pedido>';

        $data = array (
            "apikey" =>  $apikey,
            "xml" => rawurlencode($xml)
        );
        
        
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, 'https://bling.com.br/Api/v2/pedido/json/'. '&apikey=' . $apikey);
        curl_setopt($curl_handle, CURLOPT_POST, count($data));
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curl_handle);
        curl_close($curl_handle);
        return $response;
        
    }
}